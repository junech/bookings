package config

import (
	"github.com/alexedwards/scs/v2"
	"html/template"
	"log"
)

// AppConfig is the application config
type AppConfig struct {
	InProduction	bool
	TemplateCache	map[string]*template.Template
	InfoLog			*log.Logger
	Session			*scs.SessionManager
}